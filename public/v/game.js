let game;
let gameOptions = {
    playerGravity: 1200,
    playerSpeed: 200
}
window.onload = function() {
    var gameConfig = {
        type: Phaser.AUTO,
        backgroundColor: 0x444444,
        scale: {
            mode: Phaser.Scale.FIT,
            autoCenter: Phaser.Scale.CENTER_BOTH,
            parent: "thegame",
            width: 640,
            height: 480
        },
        physics: {
            default: "arcade",
            arcade: {
                gravity: {
                    y: 0
                }
            }
        },
        scene: playGame
    }
    game = new Phaser.Game(gameConfig);
}
class playGame extends Phaser.Scene{
    constructor(){
        super("PlayGame");
    }
    preload(){
        this.load.tilemapTiledJSON("level", "level.json");
        this.load.image("tile", "tile.png");
        this.load.image("hero", "hero.png");
    }
    create(){
        this.gravityDirection = 1;
        this.map = this.make.tilemap({
            key: "level"
        });
        let tile = this.map.addTilesetImage("tileset01", "tile");
        this.map.setCollision(1);
        this.layer = this.map.createStaticLayer("layer01", tile);
        this.hero = this.physics.add.sprite(300, 376, "hero");
        this.hero.body.gravity.y = gameOptions.playerGravity;
        this.canFlipGravity = true;
        this.cameras.main.setBounds(0, 0, 1920, 1440);
        this.cameras.main.startFollow(this.hero);
        this.cursors = this.input.keyboard.createCursorKeys();
        let spaceKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
        spaceKey.on("down", function(key, event){
            if(this.canFlipGravity){
                this.hero.body.gravity.y *= -1;
                this.canFlipGravity = false;
                this.hero.flipY = this.hero.body.gravity.y < 0
            }
        }, this);
    }
    update(){
        this.hero.body.velocity.y = Phaser.Math.Clamp(this.hero.body.velocity.y, -800, 800);
        this.hero.body.velocity.x = this.cursors.left.isDown ? (this.cursors.right.isDown ? 0 : -1 * gameOptions.playerSpeed) : (this.cursors.right.isDown ? gameOptions.playerSpeed : 0);
        this.hero.flipX = this.hero.body.velocity.x == 0 ? this.hero.flipX : (this.hero.body.velocity.x > 0 ? false : true);
        this.physics.world.collide(this.hero, this.layer, function(hero, layer){
            if(hero.body.blocked.down || hero.body.blocked.up){
                this.canFlipGravity = true;
            }
        }, null, this);
    }
}
