var Y = 5;
var X = Y*8;

var playState = {

	create: function() {
        /*
		this.moveSound = game.add.audio('move');
		this.moveSound.volume = 0.05;
		this.boomSound = game.add.audio('boom');
		this.boomSound.volume = 0.05;
		this.okSound = game.add.audio('ok');
		this.okSound.volume = 0.05;
		this.zik = game.add.audio('zik');
		this.zik.volume = 0.2;
		this.zik.loop = true;
		this.zik.play();
		game.add.image(0, 0, 'bg');
        */

		this.addRain('rain');

		this.tuto = game.add.text(game.width/2,40,"", { font: '20px Arial', fill: '#fff' });
		this.tuto.anchor.setTo(0.5, 0.5);

		this.level = 0;
		this.death = 0;
		this.mleft = 100;
		this.mtop = 95;
		this.cursor = game.input.keyboard.createCursorKeys();
		this.canMove = true;
		this.fruits = [];

		this.altText = game.add.text(game.width/2, game.height-35, '', { font: '20px Arial', fill: '#fff' });
		this.altText.anchor.setTo(0.5, 0.5);

		this.spikes = game.add.group();
		this.spikes.createMultiple(50, 'spike');

		this.snake = [];
		this.snaketmp = game.add.sprite(0, 0, 'snakeBody');
		this.snaketmp.anchor.setTo(0.5, 0.5);
		this.snaketmp.reset(-100, -100);

		this.snakes = game.add.group();
		this.snakes.createMultiple(15, 'snakeBody');
		this.snake[0] = game.add.sprite(0, 0, 'snakeHead');

		this.grass = game.add.group();
		this.grass.createMultiple(50, 'grass');
		this.grass1 = game.add.group();
		this.grass1.createMultiple(50, 'grass1');
		this.grass2 = game.add.group();
		this.grass2.createMultiple(50, 'grass2');
		this.grass3 = game.add.group();
		this.grass3.createMultiple(50, 'grass3');

		this.bar = game.add.group();
		this.grass3.createMultiple(50, 'grass3');

		this.dirt = game.add.group();
		this.dirt.createMultiple(50, 'dirt');

		this.food = game.add.group();
		this.food.createMultiple(25, 'food');

		this.end = game.add.sprite(-100, -100, 'end');
		this.end.anchor.setTo(0.5, 0.5);

		this.emitter = game.add.emitter(0, 0, 100)
		this.emitter.makeParticles('pixel')
		this.emitter.setYSpeed(-300, 300);
		this.emitter.setXSpeed(-300, 300);

		this.loadLevel(false);
	},

	update: function() {

		if (this.canMove) {
			tmp = true;
			for (var i = 0; i < this.snake.length; i++) {
				if (!this.isEmpty(this.snake[i].x, this.snake[i].y+X, true)) {
					tmp = false;
					break;
				}
			}

			if (tmp) {
				this.moveDown();
			}
			else
				this.inputs();
		}

		this.emitter.forEachAlive(function(e){
			e.alpha = game.math.clamp(e.lifespan / 200, 0, 1);
		}, this);
	},

	moveDown: function() {
		if (!this.input)
			return;

		for (var i = this.snake.length-1; i >= 0; i--) {
			this.moveSnake(i, 0, X, true);
		}
	},

	getIJ: function(x, y) {
		newX = Math.floor((x-this.mleft)/X);
		newY = Math.floor((y-this.mtop)/X);

		return {i : newX, j : newY};
	},

	caanMove: function(x, y) {
		one = this.isEmpty(x, y);
		two = this.isTail(x, y);

		return one && !two;
	},

	cantMove: function() {
		up = this.caanMove(this.snake[0].x, this.snake[0].y -X);
		down = this.caanMove(this.snake[0].x, this.snake[0].y +X);
		left = this.caanMove(this.snake[0].x -X, this.snake[0].y);
		right = this.caanMove(this.snake[0].x +X, this.snake[0].y);

		return !(up || down || left || right);
	},

	inputs: function() {

		if (this.input) {

		if ((this.cursor.up.isDown || this.cursor.down.isDown || this.cursor.right.isDown || this.cursor.left.isDown) && this.cantMove())
			this.killSnake();

		if (this.cursor.up.isDown && this.caanMove(this.snake[0].x, this.snake[0].y -X)) {
			//this.moveSound.play();
			this.moveSnakes();
			this.moveSnake(0, 0, -X);
			this.snake[0].rotation = 0;
		}
		else if (this.cursor.down.isDown && this.caanMove(this.snake[0].x, this.snake[0].y +X)) {
			//this.moveSound.play();

			this.moveSnakes();
			this.moveSnake(0, 0, +X);
			this.snake[0].rotation = Math.PI;
		}
		else if (this.cursor.left.isDown && this.caanMove(this.snake[0].x -X, this.snake[0].y)) {
			//this.moveSound.play();

			this.moveSnakes();
			this.moveSnake(0, -X, 0);
			this.snake[0].rotation = -Math.PI/2;
		}
		else if (this.cursor.right.isDown && this.caanMove(this.snake[0].x +X, this.snake[0].y)) {
			//this.moveSound.play();

			this.moveSnakes();
			this.moveSnake(0, +X, 0);
			this.snake[0].rotation = Math.PI/2;
		}

		}
	},

	moveSnake: function(i, x, y, fall) {
		fall = fall || false;

		newwX = this.snake[i].x + x;
		newwY = this.snake[i].y + y;



		if (i == this.snake.length-2) {
			this.snaketmp.x = this.snake[i].x;
			this.snaketmp.y = this.snake[i].y;
		}

		if (fall) {
			this.snaketmp.reset(-100, -100);
		}


		if (i==0 || i == this.snake.length-1 || fall) {
			if (fall)
				speed = 100;
			else
				speed = 150;

			game.add.tween(this.snake[i])
				.to({x : this.snake[i].x + x, y : this.snake[i].y + y}, speed)
					.start();
		}
		else {
			this.snake[i].x += x;
			this.snake[i].y += y;
		}


		if (this.canMove && fall) {
			this.canMove = false;
			game.time.events.add(160, function(){
				this.canMove = true;
			}, this);
		}
		else if (this.canMove) {
			this.canMove = false;
			game.time.events.add(250, function(){
				this.canMove = true;
			}, this);
		}

		if (this.isSpike(newwX, newwY)) {
			this.killSnake();
		}
		else if (i == 0 && this.isFood(newwX, newwY)) {
			//this.okSound.play();
			if (this.fruits.length > 0) {
				this.snake[this.snake.length] = this.snakes.getFirstDead();
				this.snake[this.snake.length-1].anchor.set(0.5, 0.5);
				this.snake[this.snake.length-1].reset(this.snake[this.snake.length-2].x, this.snake[this.snake.length-2].y);
			}
		}

		if (this.fruits.length==1 && this.level == 14 && this.rain.alpha == 0.7) {
			this.rain.destroy();
			this.addRain('food');
		}

		if (this.fruits.length==0) {
			if (!this.input)
				return;

			this.input = false;
			//this.winSound.play();

			game.time.events.add(150, function(){
				tween = game.add.tween(this.snake[0].scale).to( { x:180, y:180 }, 300).start();
				tween.onComplete.add(function(){
					game.add.tween(this.snake[0].scale).to( { x:1, y:1 }, 300).start();
				}, this);
			}, this);
			game.time.events.add(500, function(){
				this.nextLevel();
			}, this);
		}
	},

	killSnake: function() {
		if (!this.input)
			return;

		this.death++;
		//this.boomSound.play();
		this.input = false;

		game.time.events.add(150, function(){
			this.snaketmp.reset(-100, -100);
			for (var j = 0; j < this.snake.length; j++) {
				this.emitter.x = this.snake[j].x;
				this.emitter.y = this.snake[j].y;
				this.emitter.start(true, 500, 0, 15);

				this.snake[j].kill();
			}
		}, this);

		game.time.events.add(1000, function(){
			this.resetLevel(false);
		}, this);
	},

	moveSnakes: function() {
		for (var i = this.snake.length -1; i >= 1; i--) {
			console.log('movesnake:',i);
			this.moveSnake(i, this.snake[i-1].x - this.snake[i].x, this.snake[i-1].y - this.snake[i].y);
		}
	},

	isEmpty: function(x, y, enemy) {
		enemy = enemy || false;

		tmp = this.getIJ(x, y);

		if (tmp.j >= map[0].length || tmp.i >= map[0][0].length)
			return true;
		else {

			if (enemy) {
				if (map[this.level][tmp.j][tmp.i] == 4) {
					lol = true;
					this.fruits.forEach(function(e){
						if (e.poz.i == tmp.j && e.poz.j == tmp.i)
							lol = false;
					}, this);

					return lol;
				}
				else
					return (map[this.level][tmp.j][tmp.i] != 1);
			}
			else
				return (map[this.level][tmp.j][tmp.i] != 1);
		}
	},

	isTail: function(x, y) {
		tmp = this.getIJ(x, y);

		tmp2 = false;
		this.snake.forEach(function(e) {
			if (e.x == x && e.y == y)
				tmp2 = true;
		}, this);

		return tmp2;
	},

	isSpike: function(x, y) {
		tmp = this.getIJ(x, y);

		if (tmp.j >= map[0].length || tmp.i >= map[0][0].length)
			return false;
		else
			return (map[this.level][tmp.j][tmp.i] == 3);
	},

	isFood: function(x, y) {
		tmp = this.getIJ(x, y);

		if (tmp.j >= map[0].length || tmp.i >= map[0][0].length)
			return false;
		else if (map[this.level][tmp.j][tmp.i] == 4) {
			tmp2 = false;
			this.fruits.forEach(function(e){
				if (e.poz.i == tmp.j && e.poz.j == tmp.i) {
					tmp2 = true;
					game.add.tween(e.scale).to({x:0, y:0}, 100).start();
					game.time.events.add(110, function(){
						e.kill();
					}, this);

					this.fruits.splice(this.fruits.indexOf(e), 1);

				}

			}, this);

			return tmp2;
		}
	},

	resetLevel: function(neww) {
		this.grass.callAll('kill');
		this.grass1.callAll('kill');
		this.grass2.callAll('kill');
		this.grass3.callAll('kill');
		this.dirt.callAll('kill');
		this.spikes.callAll('kill');

		this.food.forEachAlive(function(e){
			e.tmp.stop();
		}, this);

		this.food.callAll('kill');
		this.snaketmp.reset(-100, -100);

		this.fruits = [];

		for (var i = 1; i < this.snake.length; i++) {
			this.snake[i].kill();
		}

		this.snake.splice(1, this.snake.length-1);

		this.loadLevel(neww);
	},

	nextLevel: function() {
		this.level++;
		this.resetLevel(true);
	},

	isGrass: function(i, j) {
		if (i < 0 || i >= map[this.level].length || j < 0 || j >= map[this.level][0].length)
			return false;
		else
			return (map[this.level][i][j] == 1);
	},

	buildGrass: function(i, j, neww) {
		if (this.isGrass(i-1,j))
			block = this.dirt.getFirstDead();
		else if (!this.isGrass(i,j-1) && !this.isGrass(i,j+1))
			block = this.grass3.getFirstDead();
		else if (!this.isGrass(i,j-1))
			block = this.grass1.getFirstDead();
		else if (!this.isGrass(i,j+1))
			block = this.grass2.getFirstDead();
		else
			block = this.grass.getFirstDead();

		block.anchor.set(0.5, 0.5);
		block.reset(this.mleft + j*X, this.mtop + i*X);
		if (neww) this.growSprite(block);
	},

	growSprite: function(e) {
		e.scale.x = 0;
		e.scale.y = 0;
		game.add.tween(e.scale).to({x:1, y:1}, 300).start();
	},

	loadLevel: function(neww) {

		this.tuto.text = (this.level+1) + "/15";
		game.add.tween(this.tuto.scale).to({x:1.25,y:1.25},100).to({x:1,y:1},200).start();


		for (var i = 0; i < map[this.level].length; i++)
			for (var j = 0; j < map[this.level][0].length; j++)
				if (map[this.level][i][j] == 1) {
					this.buildGrass(i, j, neww);
				}
				 else if (map[this.level][i][j] == 2) {
					if (this.end.x != 0)
						game.add.tween(this.end).to( { x:this.mleft + j*X, y:this.mtop + i*X }, 500).start();
					else {
						this.end.reset(this.mleft + j*X, this.mtop + i*X);
						//game.add.tween(this.end).to( { rotation : Math.PI }, 1000).loop().start();
					}
				}
				else if (map[this.level][i][j] == 3) {
					spike = this.spikes.getFirstDead();
					spike.anchor.set(0.5, 0.5);
					spike.reset(this.mleft + j*X, this.mtop + i*X);
					if (neww) this.growSprite(spike);
				}
				else if (map[this.level][i][j] == 4) {
					blop = this.food.getFirstDead();
					blop.anchor.set(0.5, 0.5);
					blop.reset(this.mleft + j*X, this.mtop + i*X);
					blop.scale.setTo(1, 1);
					blop.tmp = game.add.tween(blop.scale);
					//blop.tmp.to({x:0.9,y:0.9}, 700).to({x:1,y:1}, 700).loop().start();
					blop.poz = {i: i, j:j};
					this.fruits.push(blop);

					if (map[this.level][i+1][j] == 1)
						blop.rotation = 0;
					else if (j+1 < map[this.level][0].length && map[this.level][i][j+1] == 1)
						blop.rotation = -Math.PI/2;
					else //if (i+1 < map.length -1 && map[this.level][i+1][j] == 0)
						blop.rotation = Math.PI;

					if (neww) this.growSprite(blop);
				}
				else if (map[this.level][i][j] >= 5) {

					if (map[this.level][i][j] != 5) {
						this.snake[map[this.level][i][j]-5] = this.snakes.getFirstDead();
					}
					else
						this.snake[map[this.level][i][j]-5].rotation = 0;

					this.snake[map[this.level][i][j]-5].anchor.set(0.5, 0.5);
					this.snake[map[this.level][i][j]-5].reset(this.mleft + j*X, this.mtop + i*X);

					if (neww) this.growSprite(this.snake[map[this.level][i][j]-5]);
				}

		this.altText.text = altText[this.level];
		this.input = true;
	},

	addRain: function(sprite) {
		this.rain = game.add.emitter(game.width/2+80, 0, 100);
		this.rain.width = 820;
		this.rain.makeParticles(sprite);
		this.rain.setYSpeed(400, 600);
		this.rain.setXSpeed(-200, -200);
		this.rain.alpha = 0.9;
		if (sprite == 'rain') {
			this.rain.minRotation = 0;
			this.rain.maxRotation = 0;
			this.rain.alpha = 0.7;
		}
		this.rain.gravity = 0;
		this.rain.start(false, 1500, 50, 0);
	}
};
