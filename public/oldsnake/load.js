
var loadState = {

	preload: function() {
		game.stage.backgroundColor = '#3498db';
		load = game.add.text(game.width/2,game.height/2,"loading...", { font: '40px Arial', fill: '#fff' });
		load.anchor.setTo(0.5, 0.5);
		
		//game.load.image('bg', 'bg.png');

		//game.load.audio('move', ['sounds/move.wav', 'sounds/move.mp3'] );
		//game.load.audio('ok', ['sounds/ok.wav', 'sounds/ok.mp3'] );
		//game.load.audio('boom', ['sounds/boom.wav', 'sounds/boom.mp3']);
		//game.load.audio('zik', ['sounds/zik.mp3', 'sounds/zik.wav']);

		var snakeHead = [
			'.222222.',
			'22222222',
			'22D22D22',
			'22D22D22',
			'22222222',
			'22222222',
			'22222222',
			'22222222'
		];

		var snakeBody = [
			'22222222',
			'22222222',
			'22222222',
			'22222222',
			'22222222',
			'22222222',
			'22222222',
			'22222222'
		];

		var grass = [
			'BBBBBBBB',
			'BBBBBBBB',
			'B66BB6BB',
			'66666666',
			'66666666',
			'66666666',
			'66666666',
			'66666666'
		];

		var grass1 = [
			'BBBBBBBB',
			'BBBBBBBB',
			'B66BB6BB',
			'B6666666',
			'B6666666',
			'66666666',
			'66666666',
			'66666666'
		];

		var grass2 = [
			'BBBBBBBB',
			'BBBBBBBB',
			'B66BB6BB',
			'6666666B',
			'6666666B',
			'66666666',
			'66666666',
			'66666666'
		];

		var grass3 = [
			'BBBBBBBB',
			'BBBBBBBB',
			'B66BB6BB',
			'B666666B',
			'B666666B',
			'66666666',
			'66666666',
			'66666666'
		];

		var dirt = [
			'66666666',
			'66666666',
			'66666666',
			'66666666',
			'66666666',
			'66666666',
			'66666666',
			'66666666'
		];

		var spike = [
			'33883337',
			'23737823',
			'83783283',
			'38383733',
			'33382773',
			'72383788',
			'83773323',
			'33233733'
		];

		var end = [
			'........',
			'........',
			'........',
			'...88...',
			'...88...',
			'........',
			'........',
			'........'
		];

		var pixel = [
			'........',
			'........',
			'........',
			'...22...',
			'...22...',
			'........',
			'........',
			'........'
		];

		var food = [
			'........',
			'.222222.',
			'.222222.',
			'.222222.',
			'.222222.',
			'.222222.',
			'.222222.',
			'........'
		];

		var food = [
			'..8888..',
			'..D8D8..',
			'..8888..',
			'.222222.',
			'.822228.',
			'.8DDDD8.',
			'..D..D..',
			'..D..D..'
		];

		var food = [
			'..7777..',
			'..7887..',
			'..7887..',
			'..7777..',
			'....A...',
			'....A99.',
			'..99A...',
			'....A...'
		];

		var rain = [
			'........',
			'........',
			'........',
			'....2...',
			'...2....',
			'........',
			'........',
			'........'
		];

		game.create.texture('snakeHead', snakeHead, Y, Y, 0);
		game.create.texture('snakeBody', snakeBody, Y, Y, 0);
		game.create.texture('grass', grass, Y, Y, 0);
		game.create.texture('grass1', grass1, Y, Y, 0);
		game.create.texture('grass2', grass2, Y, Y, 0);
		game.create.texture('grass3', grass3, Y, Y, 0);
		game.create.texture('dirt', dirt, Y, Y, 0);
		game.create.texture('spike', spike, Y, Y, 0);
		game.create.texture('end', end, Y, Y, 0);
		game.create.texture('food', food, Y, Y, 0);
		game.create.texture('pixel', pixel, Y, Y, 0);
		game.create.texture('rain', rain, Y, Y, 0);
	},

	create: function() { 
		game.state.start('play');
	},

};

